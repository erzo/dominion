# Transport- und Aufbewahrungsbox
![](img/fertig/1-geschlossen.jpg)
![](img/fertig/2-halb-geöffnet.jpg)
![](img/fertig/3-geöffnet-ausgebreitet.jpg)
![](img/fertig/4-geöffnet-zusammengeklappt.jpg)

# Benötigtes Material

- Pappe 45cm × 45cm (1,5mm dick)
- 1 Magnetplatte A4
- Stoff für die innere Verkleidung 60cm × 80cm
- Kunstleder für die äußere Verkleidung 50cm × 40cm (1mm dick)
- Uhu
- Stoffkleber


# Box

Die Pappe nach dem folgenden Schnittmuster ausschneiden und falten.
Um das Falten zu erleichtern die Pappe an den gestrichelten Linien vorher mit einem scharfen Messer leicht einritzen.

![](img/schnittmuster/1-box.png)

Die Box zusammenkleben und auf einer Längskante einen Streifen der Magnetplatte ankleben.
Die Magnetplatte ist sehr glatt und man rutscht mit einem Lineal leicht ab.
Es hat sich bewährt ein Lineal mit Tesafilm an der Magnetplatte festzukleben, damit das Lineal beim Schneiden nicht verrutscht.


# Deckel

Der Deckel besteht aus einem Streifen Magnetplatte und zwei Streifen Pappe, die durch das Kunstleder und den Innenstoff zusammengehalten und an der Box befestigt werden.

1. Innenstoff bügeln, falls nötig.
2. Die beiden Pappstreifen und die Magnetplatte zurecht schneiden.
   Die Maße nicht eins zu eins aus der Skizze übernehmen, sondern an die tatsächliche Größe der Box anpassen.

   ![](img/schnittmuster/2-deckel.png)
   
3. Kunstleder und Innenstoff an einem Ende für den Deckel zurecht schneiden.
   (Das andere Ende, mit dem die Box später umschlossen werden soll, wird erst später zurecht geschnitten, wenn der Deckel fertig ist, damit Ungenauigkeiten in den nächsten Schritten nicht so tragisch sind. Falls der Stoff aber viel zu groß ist, empfiehlt es sich ihn schon mal auf eine handliche Größe zu kürzen.)
Die Maße nicht eins zu eins aus der Skizze übernehmen, sondern die Länge des Deckels gegebenenfalls an die Größe der Papp- und Magnetstreifen anpassen.

   ![](img/schnittmuster/3-stoff.png)

## Innenstoff zuschneiden
Der Innenstoff ist weich und verschiebt/verzieht sich leicht.
Daher ist es schwierig, die Schnittkanten anzuzeichnen.
Stattdessen habe ich einzelne Fäden gezogen um gerade Schnittkanten zu markieren.

1. Messen und mit einer Stecknadel markieren.
2. Mit einer Zange den Stoff fixieren, damit man den Faden nicht in die falsche Richtung zieht.
3. Mit der Stecknadel einen Faden greifen und ein Stückchen herausziehen, um eine gerade Linie zu markieren, an der man entlang schneiden kann. Dabei aufpassen, dass man den Faden nicht zerreißt.
4. Den Faden durchschneiden, damit man nur in die richtige Richtung zieht. Dabei aufpassen, dass man den Faden nicht verliert.
5. Den Faden bis zum Ende des Stoffes verschieben.
   (Bei dieser Länge ist es schwierig, den Faden komplett herauszuziehen, er reißt zu leicht. Aber wenn der Stoff nicht einfarbig ist kann es reichen den Faden ein Stückchen zu verschieben.)

## Nähen
Zum Nähen das Kunstleder auf den Innenstoff legen, die Außenseiten aufeinander.
(Mit dem Stoff oben hat sich der Stoff immer verzogen.)
Den Stoff und das Leder an den drei Schnittkanten zusammennähen.
Die vierte Kannte, an der sich der restliche Stoff befindet, mit dem der Deckel an die Box geklebt werden wird, wird nicht zusammengenäht.

![](img/IMG_20240414_215554_604.jpg)

Das Foto zeigt das Kunstleder auf dem Innenstoff mit der Innenseite noch außen nach dem Zusammennähen.
Dadrauf liegen die Papp- und Magnetstreifen, so wie sie später im Deckel liegen sollen.

## Füllen des Deckels
Den Deckel umstülpen, sodass die Außenseiten nach außen kommen. Anschließend erst den Magnetstreifen, dann den breiteren Pappstreifen und zu letzt den schmaleren Pappstreifen in den Deckel schieben.
Ein Festkleben der Streifen in dem Deckel ist nicht erforderlich.


# Deckel an der Box festkleben

Der Innenstoff ist dünn und der Kleber zieht sich leicht hindurch.
Vorher mit einem kleinen Teststreifen an einem anderen Stück Pappe üben.

1. Die Box auf den Stoff direkt neben den Deckel stellen.

   ![](img/zwischenschritte/3-1.jpg)

2. Den Deckel um die Box schließen, um den Stoff zu fixieren, und mit einem Lineal abmessen, wo der Stoff an den Kanten eingeschnitten werden muss.

   ![](img/zwischenschritte/3-2.jpg)

3. Nach den erfolderlichen Schnitten zu erst den Innenstoff auf dem Boden der Innenseite festkleben, dann zum Deckel vorarbeiten.

4. Dann den Stoff in Richtung Magnetverschluss weiter festkleben, zum Schluss die Seiten.
   An den Enden den Stoff kürzen, sodass er noch ein Stückchen über die Kante/Ecke übersteht und mit dem Kunstleder überlappen wird.
   Auch die überstehenden Enden festkleben.

5. Zum Schluss das Kunstleder an den Kanten einschneiden, an den Außenseiten der Box festkleben und die überstehenden Ränder abschneiden.


# Abstandshalter
Es empfiehlt sich einen [Abstandshalter](../box-spacer/README.md) in die Box zu setzen, den man einfach aus der Box herausziehen kann, auch wenn sie voll ist.
In diesem kann auch ein Tütchen für Spielmarker verstaut werden.
