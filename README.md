https://wiki.dominionstrategy.com/index.php?title=List_of_cards

# Zubehör für das Kartenspiel Dominion

Dieses Repository enthält eine Liste aller Karten des Kartenspiels [Dominion](https://www.dominion-welt.de/) (kopiert vom englischsprachigen [Wiki][upstream]) mit deutschen Übersetzungen und weiteren Informationen und einem LaTeX-Skript um die Informationen zu parsen und filtern.
Der ursprüngliche Zweck war die Erstellung von [Trennkarten](separator-cards.pdf) um die Spielkarten in einer [Transportbox][] besser organisieren zu können.
Als Nebenprodukt ist aber auch ein [Zufallsgenerator](pick-10-random-cards.tex) abgefallen, der 10 Königreichkarten zum spielen aussuchen kann.

Es gibt offizielle Trennkarten zum Download für [Blütezeit](https://www.dominion-welt.de/wp-content/uploads/S220084DomBluet2ndEdTrOnlineA4.pdf), [Hinterland](https://www.dominion-welt.de/wp-content/uploads/S220163DomHinterl2ndEdTrOnlineA4.pdf) und [Seaside](https://www.dominion-welt.de/wp-content/uploads/S210670DomSea2ndEdTrOnlineA4.pdf), aber leider nicht für alle Erweiterungen.
Deshalb habe ich meine eigenen gemacht.


# English version

This repository contains a list of all cards for the card game [Dominion](https://www.riograndegames.com/games/dominion/) (copied from the [wiki][upstream]) with German translations and some additional information and a LaTeX script to parse and filter this information.
The original purpose was to create [separator cards](separator-cards.pdf) to sort the playing cards into [better transportable boxes][Transportbox].
But as a side product it also produced a [random generator](pick-10-random-cards.tex) which chooses 10 kingdom cards to play with for you.


# Building the pdf containg the separator cards

A utf-8 compatible TeX engine (such as XeLaTeX) is required in order to deal with [non-breaking spaces](https://en.wikipedia.org/wiki/Nbsp) in data/cards.txt correctly.
You can build the pdf containing the separator cards by running:

```
$ xelatex separator-cards.tex
```


# Generating a random list of kingdom cards to play with

Comment out the sets that you don't have available in [pick-10-random-cards.tex](pick-10-random-cards.tex) by inserting a percent character in front of them.
Then run

```
$ xelatex pick-10-random-cards.tex
```

The output file pick-10-random-cards.pdf contains the list of cards.
If you want to keep the list copy the generated pdf file, otherwise it will be overwritten with a new list the next time you build pick-10-random-cards.tex.

If you use the option `max number sets = 2` the available cards are reduced to two random sets before picking the random cards in order to reduce the effort of getting the cards from different boxes.
Depending on the chosen sets the number of available cards printed above the table will differ.


# \InputCards

`\InputCards{...}` is defined in [preamble/read-data.tex](preamble/read-data.tex) and used in [separator-cards.tex](separator-cards.tex).

It reads the files in [data/](data/) and executes `\PrintCard` (which is defined in [preamble/print-card.tex](preamble/print-card.tex)) once for every card to be printed.

`\InputCards` takes exactly one argument which is a comma separated list of key=value pairs.
The following keys are accepted:

- keys to filter which cards/landscapes should be printed, each of these accepts a comma separated list as value, all values default to an empty token list which matches all cards/landscapes:
    - `set`: one of `Base`, `Intrigue`, `Seaside`, `Alchemy`, `Prosperity`, `Cornucopia`, `Hinterlands`, `Dark Ages`, `Guilds`, `Adventures`, `Empires`, `Nocturne`, `Renaissance`, `Menagerie`, `Allies`, `Plunder` or `Promo`
    - `type`: e.g. `Victory`, `Curse`, `Treasure`, `Action`, `Attack`, `Reaction`, `Duration`, `Night`
    - `name`: e.g. `Village`, `Gold`
    - `edition`: `1`, `2` or `` for all
- boolean keys:
    - `skip landscapes`
    - `skip non-supply`
    - `skip base cards`
- other keys:
    - `group piles`: If `true` print only one card/landscape per pile, if `false` print every card/landscape, see [Syntax of groups file](#syntax-of-groups-file).
    - `language`: currently only `de` for German translations.
      If you want English names only you need to change `\PrintCard` to use the original names only and no translations.
      If you want a different language see [Contributing](#contributing).
    - `mats`: If `true` print mats to organize tokens or cards, if `false` do not print mats, see [Syntax of mats file](#syntax-of-mats-file).
    - `randomizer`: If `true` print separator cards for [randomizer cards](https://wiki.dominionstrategy.com/index.php/Special_cards#Randomizers), one for each set.
    - `number players`: How often each mat should be printed, see [Syntax of mats file](#syntax-of-mats-file).
      If you want more than six you need to define new colors in [preamble/print-card.tex](preamble/print-card.tex) with `\NewCardColor{Player N} {r,g,b} {black}` where `N` is an integer, `r,g,b` is the color and `black` is the text color.

If the value contains a comma it must be wrapped in curly braces to protect it from being broken apart when splitting the keys.


# \PrintCard and \PrintMat

`\PrintCard` is called by `\InputCards` once for every card/landscape (or pile if `group piles` is given).
`\InputCards` also calls `\PrintMat` after all `\PrintCard` if `mats=true`.
You can redefine `\PrintCard` to change how cards/landscapes are printed to the pdf and `\PrintMat` to change how mats are printed to the pdf.

`\PrintCard` and `\PrintMat` do not take any arguments.
Instead the following commands are available to access the values:

- `\OriginalCardName{ + }` and `\TranslatedCardName{ + }` give the name of the card.
  They take one argument which is used as separator in case several cards are grouped together
  (either because it's a [split pile](https://wiki.dominionstrategy.com/index.php/Split_pile)
   or because it's several piles belonging together, e.g. [Vampire](https://wiki.dominionstrategy.com/index.php/Vampire) and [Bat](https://wiki.dominionstrategy.com/index.php/Bat),
   but [Artifacts](https://wiki.dominionstrategy.com/index.php/Artifacts) are not included, see below).
- `\OriginalCardType{ -- }` and `\TranslatedCardType{ -- }` give the types of the card, separated by the argument.
- `\IsCardType{Attack}{true}{false}` checks if the card is an attack card.
- `\IfCardIsBaseCard{true}{false}` checks if the card is a [base card](https://wiki.dominionstrategy.com/index.php/Basic_cards).
- `\OriginalCardSet{, }` and `\TranslatedCardSet{, }` give the set in which the card is included.
  In case of a [mat](https://wiki.dominionstrategy.com/index.php/Mat#Player_mats) ([coffers](https://wiki.dominionstrategy.com/index.php/Coffers)) this can be several sets and the argument is used as separator.
- `\CardSetIconMapWithSep{\includegraphics[height=1em]{#1}}{/}` can be used to include the icon of the set.
  In case of a [mat](https://wiki.dominionstrategy.com/index.php/Mat#Player_mats) ([coffers](https://wiki.dominionstrategy.com/index.php/Coffers)) this can be several sets.
  The first argument is code called once for each icon, the parameter `#1` is replaced with the file name of the icon.
  The second argument is used as separator.
- `\OriginalCardDescription` and `\TranslatedCardDescription` give a description for some piles and mats.
  Note that although there is a distinction between `card-descriptions` and `mat-descriptions` in translation files, `\PrintCard` and `\PrintMat` use the same command to access the description.

- `\CardColorOne` and `\CardColorTwo` contain a token string which can be used as part of a color name:
  `\colorbox{fill-\CardColorOne}{\textcolor{text-\CardColorOne}{Text}}`.
  Some cards (e.g. [Mill](https://wiki.dominionstrategy.com/index.php/Mill)) have two colors.
  In that case `\CardColorOne` represents one of the colors and `\CardColorTwo` represents the other.
  If a card has only one color `\CardColorOne` and `\CardColorTwo` are equal.

- `\CardCost` expands to a token list representing the cost of a card which can contain the commands `\FormatCoins{3}`, `\FormatPotions{1}` and `\FormatDebt{6}`.
  `\CardCost` expands to an empty token list if a landscape does not have any cost.

- `\OriginalCardArtifactMapWithSep{\framebox{#1}}{ }` and `\TranslatedCardArtifactMapWithSep{\framebox{#1}}{ }` give the name of [Artifacts](https://wiki.dominionstrategy.com/index.php/Artifacts) belonging to the card.
  The first argument is code called once for each artifact belonging to the card, the parameter `#1` is replaced with the name of the artifact.
  The second argument is used as separator in case several artifacts belong to the same card ([Border Guard](https://wiki.dominionstrategy.com/index.php/Border_Guard)).
- `\IfCardHasArtifact{true}{false}` can be used to check if a card has an artifact.

You can use `\DominionTranslate{card}{village}` to translate an original word to the language specified with `\InputCards{language=de}`.
But `\DominionTranslate{card}{\OriginalCardName{ + }}` fails if there are several names so you should use the specialized `\TranslatedCard...` commands instead.
The second argument of `\DominionTranslate` is the word to be translated.
The first argument of `\DominionTranslate` is the context of the word, i.e. `card` for the name of a card, `set` (e.g. for Intrigue or Base), `type` for the type of a card (e.g. Action or Victory), `mat-description` for the description of a mat or `card-description` for the description of a card or pile. Note that in contrast to the [translation file](#syntax-of-translation-files) the context is a singular form here.

You can redefine `\BeforeInputCards` and `\AfterInputCards` to do something before the first call/after the last call of `\PrintCard` within the same group.


# Syntax of translation files

Translation files list translations for the English words, one translation per line.
A colon followed by a space is used as separator, everything before it is the English word, everything after it is it's translation.

Translations must be grouped by context.
The context is given in a line starting with a `#` (similar to titles in mark down).
The first word of the context must be one of the following, the rest of the line is ignored and can be used to give more details, e.g. `# cards in Intrigue`.

- `types`: translations for card types, such as `Action`, `Treasure`, `Victory`, `Event`, `Way`
- `sets`: translations for set names such as `Base`, `Intrigue`, `Seaside`
- `cards`: translations for card names such as `Gold`, `Silver`, `Village`
- `card-descriptions`: translations for descriptions of cards or piles which can be used to explain how many cards should be used or whether a pile needs to be shuffled
- `mat-descriptions`: translations for descriptions of mats which can be used to explain the meaning of tokens or special rules for cards placed on this mat

The context is necessary because some words are ambiguous, e.g. *Menagerie* is both the name of a card and the name of a set but I don't know whether the card and set are called the same in all languages.

Spaces follow the usual (La)TeX rules, i.e. several spaces are equivalent to a single space, so translations can be aligned if desired.

`%` are comment characters following the usual (La)TeX rules, i.e. comment characters and every thing following them until the end of the line is ignored.

LaTeX commands can be used in the translations as usual.

Empty lines are ignored.

(Catcodes are like in a normal LaTeX document, except for `#` and `:` being letters and end of lines being ignored.)


# Syntax of plurals file

[data/plurals.txt](data/plurals.txt) lists irregular plural forms of English words, one word pair per line.
A colon followed by a space is used as separator, everything before it is the singluar form, everything after it is it's plural form.

The plural forms are needed when landscapes are grouped e.g. Hex → Hexes.
If the plural form is the singular form + s (e.g. Event → Events) it does *not* need to be included in this file.

This is *not* used for split piles defined with `\GroupType`, there an irregular plural form is given in the key=value list with the `name pl` key.
See [Syntax of groups file](#syntax-of-groups-file).

The syntax is the same as for [translation files](#syntax-for-translation-files) except that there are no titles/groups.


# Syntax of groups file

[data/groups.tex](data/groups.tex) is a normal TeX file.
It defines which cards/landscapes are grouped.
The following commands can/must be used:

- `\CardTypes{Action, Treasure, ...}` The argument is a comma separated list of card types.
  Anything which has a type contained in this list is a card that players can have in their deck.
  This includes
  [base cards](https://wiki.dominionstrategy.com/index.php/Basic_cards) (such as [Estate](https://wiki.dominionstrategy.com/index.php/Estate) and [Gold](https://wiki.dominionstrategy.com/index.php/Gold)),
  [kingdom cards](https://wiki.dominionstrategy.com/index.php/Kingdom_card) (such as [Village](https://wiki.dominionstrategy.com/index.php/Village) and [Witch](https://wiki.dominionstrategy.com/index.php/Witch)) and
  [non-supply cards](https://wiki.dominionstrategy.com/index.php/Supply#Non-Supply) (such as [Bag of Gold](https://wiki.dominionstrategy.com/index.php/Bag_of_Gold) and [Bat](https://wiki.dominionstrategy.com/index.php/Bat)).
  This list is used for error detection.
  If a card belongs neither to this list nor to the landscapes an error is printed.
- `\LandscapeTypes{Event, Landmark, ...}` The argument is a comma separated list of types.
  Anything which has a type contained in this list [is not considered a card](https://wiki.dominionstrategy.com/index.php/Landscape).
  This includes [Events](https://wiki.dominionstrategy.com/index.php/Event), [Ways](https://wiki.dominionstrategy.com/index.php/Way) and more.
  If `group piles` is passed to `\InputCards` in [separator-cards.tex](separator-cards.tex) everything belonging to the same landscape type is grouped together.
  Also this is needed for the filter `skip landscapes`.
- `\BaseCards{Copper, Silver, Gold, Estate, ...}` The argument is a comma separated list of card names.
  Any card contained in this list is considered a [base card](https://wiki.dominionstrategy.com/index.php/Basic_cards) and skipped with the `skip base cards` filter.
- `\GroupName{Encampment, Plunder}{type=Action - Treasure, cost=\FormatCoins{2/5}}`
  The first argument is a comma separated list of card names belonging to the [same pile](https://wiki.dominionstrategy.com/index.php/Split_pile).
  The second argument is a key=value list specifying the combined pile.
- `\GroupType{Ruin}{type=Action, cost=\FormatCoins{0}, shuffled=true}`
  The first argument is a card type indicating that all cards of this type belong to the [same pile](https://wiki.dominionstrategy.com/index.php/Split_pile).
  The second argument is a key=value list specifying the combined pile.
- `\GroupNameType{Necromancer}{Zombie} {}`
  The first argument is the name of a card.
  The second argument is a type of cards belonging to that card.
  The third argument is a key=value list which can be used to override values of the group.
- `\TravellerUpgrade{Page}{Treasure Hunter, Warrior, Hero, Champion}`
  The first argument is the name of a [Traveller](https://wiki.dominionstrategy.com/index.php/Traveller) card.
  The second argument is a comma separated list of [upgrades](https://wiki.dominionstrategy.com/index.php/Traveller) for that card.
- `\Heirloom{Cemetery}{Haunted Mirror}`
  The first argument is the name of a card.
  The second argument is the name of a [Heirloom](https://wiki.dominionstrategy.com/index.php/Heirloom) belonging to that card.
- `\Artifacts{Border Guard}{Horn, Lantern}`
  The first argument is the name of a card.
  The second argument is a comma separated list of [artifacts](https://wiki.dominionstrategy.com/index.php/Artifact) belonging to that card.


# Syntax of mats file

[data/mats.tex](data/mats.tex) is a normal TeX file, except that line breaks are ignored (instead of inserting a space or `\par`).
It defines mats which can be used to organize tokens and cards.
The following command can be used:

- `\Mat {name=Coffers, set={Guilds, Cornucopia, Renaissance}, description={During your buy phase, before you buy anything, you may remove tokens from this for +\FormatCoins{1}\! each.}}`
  It takes exactly one argument, a comma separated list of key=value pairs.
  The following keys are supported:
  - `name`: The name of the mat.
  - `description`: An optional description to be printed on the mat.
  - `global mat`: If `true` print one mat for all players, if `false` print one mat for each player.
  - `set`: A comma separated list of sets where this mat belongs to.
  - `edition`: `1`, `2` or `1,2`

  If a value contains a comma it must be wrapped in curly braces to protect it from being broken apart when splitting the keys.


# Syntax of cards file

[data/cards.txt](data/cards.txt) is copied from the [dominion wiki](https://wiki.dominionstrategy.com/index.php?title=List_of_cards&action=edit).


# Contributing

If you find an error please open an [issue](https://gitlab.com/erzo/dominion/-/issues) or even better fix it and create a merge request.

- If the data is wrong (e.g. costs):
  1. Fix it [upstream][], it's a wiki that anyone can edit. Click on [Edit][], search for the card, fix the data, copy the entire table, enter a summary, click Show Preview and if the change is rendered correctly click Save Page.
  2. Replace [data/cards.txt](data/cards.txt) with the updated version of the table.

- If a translation is wrong:
  1. Fix it in [data/translations-de.txt](data/translations-de.txt), see [Syntax of translation files](syntax-of-translation-files).
     Please note that in the German version some card names have been changed from a male/base form to an explicitly female form over time. Depending on the Dominion version you have the gender can differ.

- If an English plural form is wrong:
  1. Declare it in [data/plurals.txt](data/plurals.txt), see [Syntax of plurals file](syntax-of-plurals-file).

- If the description of a mat is wrong:
  1. Fix the English description in [data/mats.tex](data/mats.tex).
  2. Fix the translation in [data/translations-de.txt](data/translations-de.txt).

- When a new set is released:
  1. Let's hope someone has already entered the data [upstream][].
     Click on [Edit][], copy the entire table, click on Cancel.
     Replace [data/cards.txt](data/cards.txt) with the updated version of the table.
  2. Update `\CardTypes`, `\LandscapeTypes` and `\BaseCards` in [data/groups.tex](data/groups.tex) if necessary, see [Syntax of groups file](syntax-of-groups-file).
  3. Declare split piles or piles belonging together in [data/groups.tex](data/groups.tex), see [Syntax of groups file](syntax-of-groups-file).
  4. Add new mats in [data/mats.tex](data/mats.tex) if necessary, see [Syntax of mats file](syntax-of-mats-file).
  5. Add translations for card names and the set name in [translations-de.txt](translations-de.txt) and if necessary translations for new card/landscape types, card/landscape names in plural form and descriptions, see [Syntax of translation files](syntax-of-translation-files).

- If you want to add translations for a new language:
  1. Copy [translations-de.txt](translations-de.txt), name the new file based on the [ISO 639 2-letter language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
  2. Replace all German translations (the words after the colons) with the language you want, see [Syntax of plurals file](syntax-of-plurals-file).
     Some translations are available on the [wiki][upstream] page for the corresponding card in the section [Other language versions](https://wiki.dominionstrategy.com/index.php/Cellar#Other_language_versions).
     So if you are lucky you can just copy-paste the translations from there.


# License

This work is licensed under [CC BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/4.0/).


[upstream]: https://wiki.dominionstrategy.com/index.php?title=List_of_cards
[Edit]: https://wiki.dominionstrategy.com/index.php?title=List_of_cards&action=edit
[Transportbox]: ./box/README.md
