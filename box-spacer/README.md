# Abstandshalter

![](img/fertig.jpg)

Der Abstandshalter hat zwei Aufgaben:

- Wenn die Box voll ist, ist es schwierig Karten herauszuziehen.
  Der Abstandshalter hat einen Griff, an dem man ihn einfach heraus ziehen kann.
  Wenn man den Abstandshalter herausgezogen hat, kann man auch die Karten einfach herausziehen.
- In dem Abstandshalter kann ein Tütchen mit Spielsteinen (z.B. [Dorfbewohner][], [Taler][], [Schulden][]) aufbewahrt werden.


# Schnittmuster für die Pappe

![](img/schnittmuster.png)

[Taler]: https://wiki.dominionstrategy.com/index.php/Coffers
[Dorfbewohner]: https://wiki.dominionstrategy.com/index.php/Villagers
[Schulden]: https://wiki.dominionstrategy.com/index.php/Debt
